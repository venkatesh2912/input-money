/**
 * @author Faizal Vasaya
 * The model for notification
 */
export class Notification {
    public id: number;
    public actionTitle: string;
    public actionUrl: string;
    public createdDate: string;
    public closedDate: string;
    public message: string;
    public notificationRead: boolean;
    public userId: string;
    public fileName: string;

    constructor(
        id: number = 0,
        actionTitle: string = '',
        actionUrl: string = '',
        createdDate: string = '',
        closedDate: string = '',
        message: string = '',
        notificationRead: boolean = false,
        userId: string = '',
        fileName: string = ''
    ) {
        this.id = id;
        this.actionTitle = actionTitle;
        this.actionUrl = actionUrl;
        this.createdDate = createdDate;
        this.closedDate = closedDate;
        this.message = message;
        this.notificationRead = notificationRead;
        this.userId = userId;
        this.fileName = fileName;
    }
}