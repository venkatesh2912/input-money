/**
 * @author Faizal Vasaya
 * @description A component that deals with menu displayed in the topbar. It is a child component of <fhlby-topbar>.
 */
import { Component, OnInit, Inject, Input } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
// --------------------------------------- //

import { MenuModel } from './menu.model';

declare var $: any;

@Component({
  selector: 'fhlbny-menu',
  templateUrl: './menu.component.html'
})
export class MenuComponent {

  @Input()
  public menuList;

  constructor() {
  }

  ngAfterViewInit() {

    $('.nav-link').on('click',function( e ) {
      if($('.dropdown-submenu .dropdown-menu').hasClass('show')){
          $('.dropdown-submenu .dropdown-menu').removeClass('show');
      }
    } );

    $('.dropdown-menu a.dropdown-toggle').on('click', function (e) {
      var $el = $(this);
      var $parent = $(this).offsetParent(".dropdown-menu");
      if (!$(this).next().hasClass('show')) {
        $(this).parents('.dropdown-menu').first().find('.show').removeClass("show");
      }
      var $subMenu = $(this).next(".dropdown-menu");
      $subMenu.toggleClass('show');

      $(this).parent("li").toggleClass('show');

      $(this).parents('li.nav-item.dropdown.show').on('hidden.bs.dropdown', function (e) {
        $('.dropdown-menu .show').removeClass("show");
      });

      if (!$parent.parent().hasClass('navbar-nav')) {
        $el.next().css({ "top": $el[0].offsetTop, "left": $parent.outerWidth() - 4 });
      }
      return false;
    });
  }

  activeMainMuenu() {
    var listItems = $(".mainMenu li a");
    listItems.each(function (idx, li) {
      if ($(this).hasClass('active')) {
        $('.active').parents('li').addClass('active');
      }
    });
  }
  acivateMenu() {
    $(".mainMenu").removeClass('active');
  }

}
