/**
 * @author Faizal Vasaya
 * @description A component that deals with topbar. It has <fhlbny-menu>, <fhlbny-notification>, <fhlbny-usermenu> as its child components.
 */
import { Component, OnInit, Inject, Input, ViewChild, Output, EventEmitter } from '@angular/core';
// --------------------------------------- //

import { MenuModel } from '../menu/menu.model';
import { NotificationComponent } from '../notification/notification.component';
import { notificationSlide } from '../notification/notification.animation';
import { EnvironmentConfigService } from '@fhlbny-ui-commons/core';

@Component({
  selector: 'fhlbny-topbar',
  templateUrl: './topbar.component.html',
  styleUrls: ['./topbar.component.scss'],
  animations: [
    notificationSlide
  ]
})
export class TopbarComponent {
  // The name of the application to be displayed on the top bar
  @Input() appName: string;
  // The configuration for menu, to be passed to fhlbny-menu child component
  @Input() mainMenuList: MenuModel[];
  // Event emitter to emit user logout action
  @Output()
  public userLoggedOut: EventEmitter<boolean> = new EventEmitter<boolean>();

  @ViewChild(NotificationComponent) notificationComponent: NotificationComponent;

  // Current baseurl
  public baseUrl: string;

  constructor(
    private environmentConfigService: EnvironmentConfigService
  ) {
    this.baseUrl = this.environmentConfigService.environment.baseUrl;
  }

  ngOnInit() {
    if (this.notificationComponent) {
      this.notificationComponent.state = 'slideOut';
    }
    this.notificationComponent.state = 'slideOut';
  }

  // opens the notification pane (Added by: Shezad)
  openNotification() {
    if (!this.notificationComponent) {
      return;
    }
    this.notificationComponent.state = 'slideIn';
  }

  /**
     * Routes the user to the dashboard link as per the current base url.
     */
  routeToDashboard() {
    window.open(this.baseUrl + '/Dashboard', "_self");
  }

  /**
   * The method invoked when the userloggedout event occurs from user-menu child component.
   */
  onUserLoggedOut($event) {
    this.userLoggedOut.emit(true);
  }
}
