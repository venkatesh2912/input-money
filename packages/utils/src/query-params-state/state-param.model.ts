// A contract to set query params state for list screens
export class StateParam {
    ascending: boolean;
    predicate: string;
    itemsPerPage: number;
    page: number;
    [x: string]: any;
}