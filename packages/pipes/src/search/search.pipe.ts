/**
 * @author Faizal Vasaya
 * @class  SearchPipe
 * This pipe performs a search operation on the array of objects. It follows the below mentioned steps:
 * -> Checks whether the array is empty, if so return the same empty array.
 * -> Checks whether the filter string is empty, if so unflatten the array and pass it back.
 * -> Convert filter to uppercase for the purpose of comparing.
 * -> If the passed value is an array and filter exists, removes a field called 'id' because id should not be searched by the user.
 * -> Apply filter on the array.
 */
import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
    name: 'search'
})
export class SearchPipe implements PipeTransform {

    transform(value: any[], filter: string): any {
        // Check if the array has objects or not
        if (!value || !value.length) {
            return [];
        }
        // Check if the filter is not an empty string
        if (!filter) {
            return value;
        }

        // Convert filter value to uppercase for comparision
        filter = filter.toUpperCase();
        // Check whether filter value exists and the passed value is an array
        if (filter && Array.isArray(value)) {
            // Assuming entire array has same keys in its objects, get an array of keys.
            const keyName = Object.keys(value[0]);
            // Traverse the array of keys and remove a key value pair with key as 'id'
            keyName.forEach((element, index) => {
                if (element === 'id') {
                    keyName.splice(index, 1);
                }
            });
            const keys = keyName;
            // Logic to go through all the objects and find the objects whose value matches uppercased filter.
            value = (value.filter((v) => v && keys.some(
                (k) => v[k] === undefined || v[k] === null ? false : v[k].toString().toUpperCase().indexOf(filter) >= 0))
            );
            return value;
        }
    }
}
