/**
 * @author Faizal Vasaya
 * @description A module to register fhlbny application wide pipes.
 */
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
// --------------------------------------- //

import { SearchPipe } from './search/search.pipe';
// --------------------------------------- //

export { SearchPipe } from './search/search.pipe';

@NgModule({
    imports: [
        CommonModule
    ],
    declarations: [
        SearchPipe
    ],
    exports: [
        SearchPipe
    ]
})
export class PipesModule {
}
