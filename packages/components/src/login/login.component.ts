/**
 * @author Faizal Vasaya
 * @class  LoginComponent
 * The Login component handles the logic for user authentication using login prompt
 */
import { Component, OnInit, ViewChild, ElementRef, Renderer2 } from '@angular/core';
import { Router } from '@angular/router';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
// --------------------------------------- //
import { AuthenticationService } from '@fhlbny-ui-commons/core';

@Component({
  templateUrl: './login.component.html'
})

export class LoginComponent implements OnInit {
  @ViewChild('loginFormElement', { read: ElementRef }) loginFormElement: ElementRef;
  // The username entered by the user. This is mapped using ngModel
  public username: string;
  // The password entered by the user. This is mapped using ngModel
  public password: string;
  // tslint:disable-next-line:max-line-length
  // A boolean to deide whether the login prompt needs to be shown to the user or not. This is requried in case the user is authenticated but not assigned any roles as such.
  public showLoginPrompt: boolean;
  // A formgroup to store login form
  public loginForm: FormGroup;
  // A boolean indicating whether the form is submitted or not
  public isFormSubmitted: boolean;
  // A boolean indivating whether the credentials entered by the user are valid or not
  public isCredentialValid: boolean;


  constructor(
    private authService: AuthenticationService,
    private router: Router,
    private formBuilder: FormBuilder,
    private renderer: Renderer2
  ) { }

  ngOnInit() {
    // Set the value based on user's details
    this.showLoginPrompt = !(this.authService.user.isAuthenticated && this.authService.user.isAuthorized);
    // Route to the root if the user is athenticated as well as authorized. This is an edge case when a user manually types login in the url's address bar.
    if (this.authService.user.isAuthenticated && this.authService.user.isAuthorized) {
      this.router.navigate(['/']);
    }
    // Subscribe to userDetails to change the value of login prompt when the user logs out.
    this.authService.userDetails.subscribe(
      (userdetails) => {
        this.showLoginPrompt = !(userdetails.isAuthenticated && userdetails.isAuthorized);
      }
    );
    this.isCredentialValid = true;
    this.createLoginForm();
  }

  /**
   * Creates a form group for login
   */
  createLoginForm() {
    this.loginForm = this.formBuilder.group({
      username: ['', Validators.required],
      password: ['', Validators.required]
    });
  }

  /**
   * Invoked when the user clicks on login button. It creates a query string that needs to be sent to server for user's authentication.
   * Cases:
   * 1. User Authenticated but not Authorized -> Refer comment //1 in the method
   * 2. User Authenticated and Authorized -> Refer comment //2
   * 3. Unauthorized user -> Refer to comment //3
   */
  authenticate() {
    this.isFormSubmitted = true;
    this.renderer.addClass(this.loginFormElement.nativeElement, 'was-validated');
    if (this.loginForm.valid) {
      const queryString = `${'username=' + this.loginForm.get('username').value + '&password=' + this.loginForm.get('password').value}`;
      this.authService.authenticateUsingPrompt(queryString).then(
        (success) => {
          // 1
          if (this.authService.user.isAuthorized === false) {
           // this.toasterService.pop('error', 'Error', 'Roles are not assigned to this user!');
          } else {
            // 2
            this.router.navigate(['/']);
          }
        },
        (failure) => {
          this.isCredentialValid = false;
        }
      );
    }
  }

  validateUserInput() {
    let isValid = true;
    if ((this.username === '' || this.username === undefined) || (this.password === '' || this.password === undefined)) {
      isValid = false;
    }
    return isValid;
  }
}
