import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ValidatorFn } from '@angular/forms';

import { IsoDateFormatter } from './iso-date-formatter';
import { FilterField } from './filter-field.model';

@Component({
    // tslint:disable-next-line:component-selector
    selector: 'fhlbny-filtering',
    templateUrl: './filtering.component.html',
})
export class FilteringComponent implements OnInit {

    // tslint:disable-next-line:no-input-rename
    @Input('fields')
    filterFields: FilterField[];

    // tslint:disable-next-line:no-output-on-prefix
    @Output()
    onSearchSubmit: EventEmitter<object> = new EventEmitter<object>();

    // private form: FormGroup; // gives error in aot build
    public form: FormGroup;

    constructor() { }

    ngOnInit() {
        this.form = new FormGroup({});

        for (const field of this.filterFields) {
            field.inputType = field.inputType || 'text';

            switch (field.inputType) {
                case 'text':
                case 'number':
                case 'select':
                    const validators: ValidatorFn[] = [];

                    if (field.minLength) {
                        validators.push(Validators.minLength(field.minLength));
                    }
                    if (field.maxLength) {
                        validators.push(Validators.maxLength(field.maxLength));
                    }

                    this.form.addControl(field.name, new FormControl(field.value, validators));
                    break;
                case 'date':
                    this.form.addControl(field.name, new FormControl(IsoDateFormatter.fromIsoString(field.value)));
                    break;
                case 'dateBetween':
                    this.form.addControl(
                        `${field.name}-from`,
                        new FormControl(IsoDateFormatter.fromIsoString(field.value && field.value.from || null)));
                    this.form.addControl(
                        `${field.name}-to`,
                        new FormControl(IsoDateFormatter.fromIsoString(field.value && field.value.to || null)));
            }
        }
    }

    public search() {
        this.onSearchSubmit.emit(this.getSearchObject());
    }

    public reset() {
        this.form.reset();

        this.onSearchSubmit.emit(this.getSearchObject());
    }

    public getSearchObject(): object {
        const searchObject = {};

        for (const field of this.filterFields) {
            const fieldName = field.name;
            const inputType = field.inputType;
            const fieldValue = this.form.value[fieldName];

            switch (inputType) {
                case 'text':
                    if (this.form.value[fieldName]) {
                        searchObject[`${fieldName}.contains`] = encodeURIComponent(this.form.value[fieldName]);
                    }
                    break;
                case 'number':
                case 'select':
                    if (this.form.value[fieldName]) {
                        searchObject[`${fieldName}.equals`] = encodeURIComponent(this.form.value[fieldName]);
                    }
                    break;
                case 'date':
                    if (this.form.value[fieldName]) {
                        searchObject[`${fieldName}.equals`] = IsoDateFormatter.toIsoString(fieldValue);
                    }
                    break;
                case 'dateBetween':
                    const fromDateValue = this.form.value[`${fieldName}-from`];
                    const toDateValue = this.form.value[`${fieldName}-to`];

                    if (fromDateValue) {
                        searchObject[`${fieldName}.greaterOrEqualThan`] = IsoDateFormatter.toIsoString(fromDateValue);
                    }
                    if (toDateValue) {
                        searchObject[`${fieldName}.lessOrEqualThan`] = IsoDateFormatter.toIsoString(toDateValue);
                    }
            }
        }

        return searchObject;
    }
    public onlyNumeric(e) {
        let input;
        if (e.metaKey || e.ctrlKey) {
          return true;
        }
        if (e.which === 32) {
         return false;
        }
        if (e.which === 0) {
         return true;
        }
        if (e.which < 33) {
          return true;
        }
        input = String.fromCharCode(e.which);
        return !!/[\d\s]/.test(input);
    }
    /**
     * track for loop
     * @param index
     * @param item
     */
    name(index: number, item: FilterField): any {
        return item.name;
    }
}
