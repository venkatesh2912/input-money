import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgbDatepickerModule } from '@ng-bootstrap/ng-bootstrap';

import { FilteringComponent } from './filtering.component';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        NgbDatepickerModule.forRoot()
    ],
    declarations: [
        FilteringComponent
    ],
    exports: [
        FilteringComponent,
    ]
})
export class FilteringModule {}
