import { Component, EventEmitter, Output } from '@angular/core';
import { ExportType } from './export-type';

@Component({
    selector: 'fhlbny-export-controls',
    templateUrl: './export-controls.component.html',
})
export class ExportControlsComponent {

    @Output()
    onExport: EventEmitter<ExportType> = new EventEmitter<ExportType>();

    public availableExportTypes: any;

    constructor() {
        this.availableExportTypes = ExportType;
    }

    export(exportType: ExportType) {
        this.onExport.emit(exportType);
    }
}
