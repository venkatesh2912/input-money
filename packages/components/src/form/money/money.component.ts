import { Component, OnInit, Input, forwardRef, Type, HostListener, ElementRef, ViewChild } from '@angular/core';
import {ControlValueAccessor, NG_VALUE_ACCESSOR, NG_VALIDATORS, FormControl } from '@angular/forms';

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'fhlbny-money',
  templateUrl: './money.component.html',
  styleUrls: ['./money.component.scss'],
  providers: [{
    provide: NG_VALUE_ACCESSOR,
    useExisting: forwardRef(() => MoneyComponent),
    multi: true
  }]
})
export class MoneyComponent implements ControlValueAccessor, OnInit {
    parts: string;
    @Input()
    label: string;
    @Input()
    control: FormControl;
    @ViewChild('myname') input: any;
    constructor( private el: ElementRef) {}
    // this is the initial value set to the component
    private propagateChange = (_: any) => { };
    public writeValue(obj: any) { }
    public registerOnChange(fn: any) { this.propagateChange = fn; }
    public registerOnTouched(fn: any) { }
    // host listener fir
    @HostListener('keyup', ['$event'])
    keyEvent($event) {
        // Event target value set the last character
        this.propagateChange( $event.target.value);
        // control value get all words of input text
        let  parts = (this.control.value.replace(/[^0-9]/g , '')).toString().split('.');
        parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ',');
        parts = (parts.join('.')  === '') ? '' : '$' +  parts.join('.') ;
        this.control.setValue(parts, {emitModelToViewChange: true});
        this.input.nativeElement.value = parts;
    }
    ngOnInit(): void {}
}
