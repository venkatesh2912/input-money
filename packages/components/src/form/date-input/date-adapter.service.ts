import { Injectable } from "@angular/core";
import { NgbDateAdapter, NgbDateStruct } from "@ng-bootstrap/ng-bootstrap";

/**
 *A native adapter for fhlbny dates to be used along with ng bootstrap datepicker
 */
@Injectable()
export class DateAdapterService extends NgbDateAdapter<string> {

    /**
     * Convert mm/dd/yyyy to NgbDateStruct
     */
    fromModel(date: string): NgbDateStruct {
        let dateContent: string[] = [];
        dateContent = date.split('/', 3);
        return { year: +dateContent[2], month: +dateContent[0], day: +dateContent[1] };
    }

    /**
     * Convert ngbDateStruct to string with mm/dd/yyyy
     * @param date A date in ngbdatestruct format
     */
    toModel(date: NgbDateStruct): string {
        let stringDate: string = "";
        if (date) {
            stringDate += date.month + "/";
            stringDate += date.day + "/";
            stringDate += date.year;
        }

        return stringDate;
    }
}