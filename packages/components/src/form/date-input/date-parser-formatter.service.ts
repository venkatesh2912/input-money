import { Injectable } from "@angular/core";
import { DatePipe } from "@angular/common";
import { NgbDateParserFormatter, NgbDateStruct } from "@ng-bootstrap/ng-bootstrap";

/**
 * A custom date parser formatter that extends the NgbDateParserFormatter abstract class. This is
 * required as all the fhlbny applications requires mm-dd-yyyy
 */
@Injectable()
export class DateParserFormatterService extends NgbDateParserFormatter {

    constructor(private datePipe: DatePipe) {
        super()
    }
    /**
     * Parses the given value to an NgbDateStruct. Implementations should try their best to provide a result, even
     * partial. They must return null if the value can't be parsed.
     * @param value the value to parse
     */
    parse(value: string): NgbDateStruct {
        let dateContent: string[] = [];
        dateContent = value.split('/', 3);
        return { year: +dateContent[2], month: +dateContent[0], day: +dateContent[1] };
    }

    /**
     * Formats the given date to a string. Implementations should return an empty string if the given date is null,
     * and try their best to provide a partial result if the given date is incomplete or invalid.
     * @param date the date to format as a string
     */
    format(date: NgbDateStruct): string {
        let stringDate: string = "";
        if (date) {
            stringDate += date.month + "/";
            stringDate += date.day + "/";
            stringDate += date.year;
        }
        return stringDate;
    }

}