import { AbstractControl } from "@angular/forms";
import { Select } from "./select-input/select-input.model";

export class FormDescription {
    label: string;
    control: AbstractControl;
    type: InputType;
    errorMessages?: ErrorMessage[];
    values?: Select[];
    isVisible?: boolean;
}

export enum InputType {
    string = 'string',
    number = 'number',
    date = 'date',
    select = 'select'
}

export class ErrorMessage {
    error: string;
    message: string;
}

export enum FormType {
    add = 'add',
    update = 'update'
}

export enum ButtonType {
    add = 'add',
    update = 'update',
    cancel = 'cancel',
    delete= 'delete'
}