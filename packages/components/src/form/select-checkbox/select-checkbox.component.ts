/**
 * @author Venkatesh Parihar
 * @class  SelectCheckboxComponent
 * The SelectcheckboxComponent is an multiple selection checkbox.
 */
import { Component, OnInit, Input, HostListener, ElementRef, ViewChild} from '@angular/core';
import { FormControl } from '@angular/forms';
  @Component({
    selector: 'fhlbny-select-checkbox',
    templateUrl: './select-checkbox.component.html',
    styleUrls: ['./select-checkbox.component.scss']
  })
  export class SelectCheckboxComponent implements OnInit {
    @Input() source: { id: string , name: string }[] = []; // source object declare array
    @Input()
    label: string;
    @Input()
    control: FormControl;
    @Input()
    search: FormControl;
    @ViewChild('firstLi') firstLi: ElementRef;
    constructor(private el: ElementRef) { }
    public selectedItems:  Array<{id: string , name: string , ischecked: boolean}> = [  ]; // selected object declare array
    public isOpen = false;
    public filterText: string;
    public filterPlaceholder: string;
    public filterInput = new FormControl();
    toggleDropDown = false;
    focusedIndex = 0;
    /* This method check whether object is in selected items array
     if existed then get its id and remove from selected items using splice
     if not existed than add it using push */
    select(item: any, id: number) {
        let index: number;
        for (let i = 0; i < this.selectedItems.length; i++) {
          if (this.selectedItems[i]['id'] === item.id) {
            this.selectedItems[i].ischecked = false;
              index = i; // get  id  of remove item
              break;
          }
        }
        if (index === undefined) {
          item.ischecked = true;
          this.selectedItems.push(item); // if no id get then it add it
        } else {
          this.selectedItems.splice(index, 1); // if no id get then it delete it.Index has its position in array
        }
      this.control.setValue(this.selectedItems, {emitModelToViewChange: true}); // push data in form control
      this.focusedIndex = id;
    }
    toggleSelect() {
      this.isOpen = !this.isOpen;
      this.focusedIndex = 0;
    }
    clearFilter() {
      this.filterText = '';
    }
    ngOnInit() {
      this.filterText = '';
      this.filterPlaceholder = 'Filter.. ( Min 4 character) ';
      /* it serach it after 4 character and when text box change this method called and it subscribe changes
         it push changes in filtertext which is mention in input text as [value]= filterText */
      this.filterInput
      .valueChanges
      .subscribe(term => {
        if (term.length >= 4 || term.length === 0) { // charecter more than 4 and if empty all records display
          this.filterText = term;
          this.search.setValue(term, {emitModelToViewChange: true});
        }
         this.focusedIndex = 0;
      });
    }
     // host listener fir
     public showDropDown() {
         this.toggleDropDown = true;
         setTimeout(() => {
           this.firstLi.nativeElement.focus();
         }, 10);
     }
    @HostListener('body : keyup', ['$event'])
    keyEvent($event) {
      let  str = `item-${this.focusedIndex}`;
      let  elmnt = document.getElementById(str);
      const list = document.getElementById('outlist');
      if ($event.code === 'ArrowUp') {
        $event.preventDefault();
        if (this.focusedIndex !== 0) {
          this.focusedIndex--;
          list.scrollTop = (elmnt.offsetTop - 50);
        }
      }
      if ($event.code === 'ArrowDown') {
        $event.preventDefault();
        if (this.focusedIndex <= (this.source.length - 2)) {
          this.focusedIndex++;
          list.scrollTop = (elmnt.offsetTop - 50);
        }else {
          this.focusedIndex = 0;
           str = `item-${this.focusedIndex}`;
           elmnt = document.getElementById(str);
          list.scrollTop = (elmnt.offsetTop - 50);
        }
      }
      // if ($event.code === 'Enter') {
      //   $event.preventDefault();
      //   this.toggleSelect()
      // }
    }
  }
