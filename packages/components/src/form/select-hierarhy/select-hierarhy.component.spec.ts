import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SelectHierarhyComponent } from './select-hierarhy.component';

describe('SelectHierarhyComponent', () => {
  let component: SelectHierarhyComponent;
  let fixture: ComponentFixture<SelectHierarhyComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SelectHierarhyComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SelectHierarhyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
