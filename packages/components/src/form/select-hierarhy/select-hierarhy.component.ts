import { Component, OnInit, Input } from '@angular/core';
import {ControlValueAccessor, NG_VALUE_ACCESSOR, NG_VALIDATORS, FormControl, FormGroup } from '@angular/forms';

@Component({
  selector: 'fhlbny-select-hierarhy',
  templateUrl: './select-hierarhy.component.html',
  styleUrls: ['./select-hierarhy.component.scss']
})
export class SelectHierarhyComponent implements OnInit {

  constructor() { }
  @Input()
  formGroup: FormGroup;
  @Input()
  formcontrolnames: any[];
  ngOnInit() {
    console.log('formcontrolnames', this.formcontrolnames);
  }

}
