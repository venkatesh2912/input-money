import { Component, ContentChild, Input, TemplateRef } from '@angular/core';

@Component({
    selector: 'fhlbny-panel',
    templateUrl: './panel.component.html',
})
export class PanelComponent {

    @Input()
    headerTitle: string;

    @ContentChild('additionalHeaderContent') additionalHeaderContentRef;

    constructor() {}
}
