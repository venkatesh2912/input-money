export interface TableField {
    name: string;
    label: string;
    dataType?: 'text' | 'date';
    routerLinkFn?: Function;
    [x: string]: any;
}
