/**
 * @author Faizal Vasaya
 * @function  singleSignOn
 * This method is invoked on the application's initial load. It is invoked by angular's APP_INITIALIZER token. The application
 * flow continues when if the promise resolves.
 */
import { AuthenticationService } from '../authentication/authentication.service';
import { EnvironmentConfigService } from '../environment-config/environment-config.service';

export function singleSignOn(authService: AuthenticationService, environmentConfigService: EnvironmentConfigService): () => Promise<any> {
    // Initializes the environment variables
    environmentConfigService.initializeApplicationEnvironment(window.location.hostname);
    // Authenticates the user.
    return () => authService.authenticateUser();
}
