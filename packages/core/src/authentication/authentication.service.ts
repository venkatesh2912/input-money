/**
 * @author Faizal Vasaya
 * This service consists of methods regarding changes caused to the User resources such as user details, authentication and signature
 */
import { Injectable } from '@angular/core';
import { Subject } from 'rxjs/Subject';
import { HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import 'rxjs/add/operator/map';
import { Observable } from 'rxjs/Observable';
// --------------------------------------- //
import { User, ModuleFunctionMapping, Signature } from './user.model';
import { HttpRequestService } from '../http-request/http-request.service';

@Injectable()
export class AuthenticationService {
  // An observable that emits values to confirm whether a user is authenticated or not
  public userDetails: Subject<User>;
  // A central storage to store all the user details
  public user: User;
  // An event that confirms that the non-single signed on user has refreshed the token to login as single signed on user.
  public tokenRefreshed: Subject<boolean>;
  public appName: string = 'User Services';
  // Creates an empty user object and registers it as an observable.
  constructor(private httpRequest: HttpRequestService) {
    this.userDetails = new Subject<User>();
    this.tokenRefreshed = new Subject<boolean>();
    this.user = new User();
  }

  // Emits the current status of authentication to all its subscribers
  emitUserDetails(user: User) {
    this.userDetails.next(user);
  }

  // Emits the change user event, if the application transforms from non-single signed on user to single signed on user by refreshing the token
  emitTokenRefreshed() {
    this.tokenRefreshed.next(this.user.isSSOUser);
  }

  // A get request to perform user authentication. This method is invoked to perform single sign on when the angular app is initially loaded.
  authenticateUser(): Promise<any> {
    if (this.validateLocalStorage()) {
      this.setUserDetailsFromLocalStorage(JSON.parse(localStorage.getItem('user')));
    } else {
      const authPromise = new Promise<any>(
        (resolve, reject) => {
          // Perform a get request to authenticate the user. This request will be intercepted by application wide request interceptor in request-interceptor.ts
          this.httpRequest.getRequest('/auth', this.appName).then(
            (userdetail) => {
              this.setUserDetails(userdetail, true);
              resolve();
            },
            (error: HttpErrorResponse) => {
              resolve();
              console.log(error.message);
            }
          );
        }
      );
      return authPromise;
    }
  }

  /**
   * This method is invoked when the user authentication needs to be performed using the login prompt. Note for single signon authenticate user is invoked and not
   * this method. On succesfull response from the server it persists the userdetails in an object.
   * @param requestBody The request body consists of the username and password that will sent in the post request.
   */
  authenticateUsingPrompt(requestBody): Promise<any> {
    const authPromise = new Promise<any>(
      (resolve, reject) => {
        // Perform a post request to authenticate the user. This request will be intercepted by application wide request interceptor in request-interceptor.ts
        this.httpRequest.postRequest('/login', requestBody, this.appName).then(
          (userdetail) => {
            this.setUserDetails(userdetail, false);
            resolve();
          },
          (error: HttpErrorResponse) => {
            reject(error.message);
          }
        );
      }
    );
    return authPromise;
  }

  /**
   * This method is invoked to get an observable which returns an Refreshed token for the user.
   */
  refreshToken(): Observable<any> {

    return this.httpRequest.getRequestObservable('/auth', this.appName).map(
      (userdetail) => {
        this.setUserDetails(userdetail, true);
      },
    );
  }

  /**
   * This method is invoked to get the current user's signature resource from the server. On successfull return the signature is sent to the promises's thened
   * invoker.
   */
  getUserSignature() {
    const signaturePromise = new Promise<any>(
      (resolve, reject) => {
        // Perform a get request to authenticate the user. This request will be intercepted by application wide request interceptor in request-interceptor.ts
        this.httpRequest.getRequest('/api/user-signatures/' + this.user.name.split('@')[0], this.appName).then(
          (signature: Signature) => {
            resolve(signature);
          },
          (error: HttpErrorResponse) => {
            reject(error.message);
          }
        );
      }
    );
    return signaturePromise;
  }
  /**
   * This method is invoked to upload the signature. It creates the request body as expected by the server and invokes the post request to the
   * specified url.
   * @param signature The bas64 encoded image to be uploaded to the server.
   */
  uploadSignature() {
    let requestBody: Signature = {};
    if (this.user.signature.id) {
      requestBody = {
        id: this.user.signature.id,
        modifiedDate: new Date().toISOString(),
        crteDate: this.user.signature.crteDate
      }
    } else {
      requestBody.crteDate = requestBody.modifiedDate = new Date().toISOString();
      //requestBody.modifiedDate = new Date().toISOString();
    }
    requestBody.image = this.user.signature.image;
    requestBody.userName = this.user.name.split('@')[0];

    const signaturePromise = new Promise<any>(
      (resolve, reject) => {
        // Invokes the postRequest to authenticate the user. This request will be intercepted by application wide request interceptor in request-interceptor.ts
        this.httpRequest.postRequest('/api/user-signatures', requestBody, this.appName).then(
          (response) => {
            resolve(response);
          },
          (error: HttpErrorResponse) => {
            reject(error.message);
          }
        );
      }
    );
    return signaturePromise;
  }

  /**
   * This method is used to delete the current user's signature.
   */
  deleteSignature() {
    const signaturePromise = new Promise<any>(
      (resolve, reject) => {
        // Perform a get request to authenticate the user. This request will be intercepted by application wide request interceptor in request-interceptor.ts
        this.httpRequest.deleteRequest('/api/user-signatures/' + this.user.signature.id, this.appName).then(
          (response) => {
            resolve(response);
          },
          (error: HttpErrorResponse) => {
            reject(error.message);
          }
        );
      }
    );
    return signaturePromise;
  }

  /**
   * This method is invoked to logout the currently logged in user. It clear out all the current user's details from its object and resolves
   * the promise.
   */
  logoutUser(): Promise<any> {
    let logoutPromise = new Promise<any>(
      (resolve, reject) => {
        this.emptyUserDeatils();
        resolve();
      }
    );
    return logoutPromise;
  }

  /**
   * Sets the service's user variable's value from the user details extracted from the local storage.
   * @param userdetail The user details stored in the local storage
   */
  setUserDetailsFromLocalStorage(userdetail) {
    this.user = new User(
      userdetail.accessToken,
      userdetail.roles,
      userdetail.name,
      userdetail.modules,
      userdetail.image,
      userdetail.version,
      userdetail.isAuthenticated,
      userdetail.isAuthorized,
      userdetail.moduleFunctionMapping,
      userdetail.tokenExpirationTimeStamp,
      userdetail.isSSOUser
    );
    this.emitUserDetails(this.user);
  }

  /**
   * Sets the user's details, stores it in the local storage and calls its event emitter
   * @param userdetail The user details to be set in the user object
   * @param isSSOUser True if the currently logged in user is a single signed on user, false otherwise.
   */
  setUserDetails(userdetail, isSSOUser: boolean) {
    var allowModule = [];
    if (userdetail.modules.length > 0) {
      userdetail.modules.forEach(element => {
        allowModule.push(element.module);
      });
    }
    var moduleFunctionMapping: ModuleFunctionMapping[] = [];
    if (userdetail.modules.length > 0) {
      userdetail.modules.forEach(element => {
        let mapping = new ModuleFunctionMapping(element.functionId, element.functionName, element.module);
        moduleFunctionMapping.push(mapping);
      });
    }
    this.user = new User(userdetail.accessToken,
      userdetail.roles,
      userdetail.name,
      allowModule,
      userdetail.userImage,
      userdetail.version,
      true,
      allowModule.length > 0,
      moduleFunctionMapping,
      userdetail.expirationTime,
      isSSOUser
    );
    this.setLocalStorage(this.user);
    this.emitUserDetails(this.user);
  }

  /**
   * Validates that the data required for a user to login is available in the local storage object. If the user details available in the localstorage is valid then it returns
   * a true else it returns a false. It validates the user details by comparing the expiration time with the current time.
   */
  validateLocalStorage(): boolean {
    if (localStorage.getItem('user') === null || localStorage.getItem('user') === undefined) {
      return false;
    }
    let user = JSON.parse(localStorage.getItem('user'));
    let expirationTime = user.tokenExpirationTimeStamp;
    return expirationTime > new Date();
  }

  /**
   * Sets the user details in the local storage
   */
  setLocalStorage(user: User) {
    localStorage.clear();
    localStorage.setItem('user', JSON.stringify(user));
  }

  /**
   * Emptys up the user's resource object and its local storage object on logout and emits the updated object.
   */
  emptyUserDeatils() {
    this.user = new User();
    localStorage.removeItem('user');
    this.emitUserDetails(this.user);
  }
}
