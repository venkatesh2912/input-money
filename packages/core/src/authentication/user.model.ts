
/**
 * @author Faizal Vasaya
 * A model to store user details. This maps to the response body of /UserServices/authUser API's response body.
 */
export class User {
    public accessToken: string;
    public roles: string[];
    public name: string;
    public modules: string[];
    public image: string;
    public version: string;
    public isAuthenticated: boolean;
    public isAuthorized: boolean;
    public moduleFunctionMapping: ModuleFunctionMapping[];
    public tokenExpirationTimeStamp: number;
    public isSSOUser: boolean;
    public signature: Signature;

    constructor(accessToken?: string, roles?: string[], name?: string, modules?: string[],
        image?: string, version?: string, isAuthenticated?: boolean, isAuthorized?: boolean,
        moduleFunctionMapping?: ModuleFunctionMapping[], tokenExpirationTimeStamp?: number,
        isSSOUser?: boolean, signature?: Signature
    ) {
        this.accessToken = accessToken || '';
        this.roles = roles || [];
        this.name = name || '';
        this.modules = modules || [];
        this.image = image || '';
        this.version = version || '';
        this.isAuthenticated = isAuthenticated || false;
        this.isAuthorized = isAuthorized || false;
        this.moduleFunctionMapping = moduleFunctionMapping || [];
        this.tokenExpirationTimeStamp = tokenExpirationTimeStamp || 0;
        this.isSSOUser = isSSOUser || false;
        this.signature = signature || {};
    }
}
export class ModuleFunctionMapping {
    public functionId: number;
    public functionName: string;
    public moduleName: string;

    constructor(
        functionId?: number,
        functionName?: string,
        moduleName?: string,
    ) {
        this.functionId = functionId;
        this.functionName = functionName;
        this.moduleName = moduleName;
    }
}
export class Signature {
    public id?: number;
    public image?: string;
    public userName?: string;
    public crteDate?: string;
    public modifiedDate?: string;
    constructor(id: number, image: string, userName: string, crteDate: string, modifiedDate: string) {
        this.id = id || null;
        this.image = image || '';
        this.userName = userName || '';
        this.crteDate = crteDate || '';
        this.modifiedDate = modifiedDate || '';
    }
}