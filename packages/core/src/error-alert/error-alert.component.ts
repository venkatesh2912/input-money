import { Component, ContentChild, Input, TemplateRef, OnInit } from '@angular/core';
import { ErrorAlertService } from './error-alert.service';
import { ErrorAlert } from './error-alert.model';
import { Router, NavigationStart } from '@angular/router';

@Component({
    selector: 'fhlbny-error-alert',
    templateUrl: './error-alert.component.html',
})
export class ErrorAlertComponent implements OnInit {
    showError: boolean;
    message: ErrorAlert;
    constructor(private errorAlertService: ErrorAlertService,private router: Router) {
        router.events.subscribe(event => {
            if (event instanceof NavigationStart) {
                this.showError = false;
            }
        });
    }

    ngOnInit() {
        // clear alert on route change       
        this.errorAlertService.getAlert().subscribe((err) => {
            this.message = err;
            this.showError = true;
        })
    }
}
