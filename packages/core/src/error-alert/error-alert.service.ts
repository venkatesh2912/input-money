/**
 * @author Faizal Vasaya
 * This service consists of methods to change the current status of the spinner
 */
import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
import { Observable } from 'rxjs/Observable';
import { ErrorAlert, FieldError } from './error-alert.model';
//---------------------------//

@Injectable()
export class ErrorAlertService {
    public subject: Subject<any> = new Subject<any>();
    constructor() {
    }

    showError(error: any) {
        this.subject.next(this.errorMapper(error));
    }

    getAlert(): Observable<any> {
        return this.subject.asObservable();
    }

    errorMapper(error): ErrorAlert {
        let fieldError = new Array<FieldError>();
        if (error.fieldErrors) {
            error.fieldErrors.forEach(element => {
                fieldError.push(new FieldError(element.field, element.message, element.objectName))
            });
        }
        let errorAlert = new ErrorAlert(error.status, error.path, error.message, fieldError, error.title, error.timestamp, error.error);
        return errorAlert;
    }

}
