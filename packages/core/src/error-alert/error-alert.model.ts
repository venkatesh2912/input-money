export class ErrorAlert {
    timestamp: string;
    title: string;
    status: string;
    path: string;
    error: string;
    message: string;
    fieldErrors: FieldError[];
    constructor(status: string, path: string, message: string, fieldErrors: FieldError[], title?: string, timestamp?: string, error?: string) {
        this.status = status;
        this.path = path;
        this.message = message;
        this.fieldErrors = fieldErrors;
        this.timestamp = timestamp || undefined;
        this.error = error || undefined;
        this.title = title || undefined;
    }
}

export class FieldError {
    objectName: string;
    field: string;
    message: string;
    constructor(objectName: string, field: string, message: string) {
        this.objectName = objectName;
        this.field = field;
        this.message = message;
    }
}