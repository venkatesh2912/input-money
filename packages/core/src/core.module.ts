/**
 * @author Faizal Vasaya
 * @description A module to configure and register core services.
 */
import { CommonModule } from '@angular/common';
import { NgModule, ModuleWithProviders, Optional, SkipSelf } from '@angular/core';
// --------------------------------------- //

import { RequestInterceptorService } from './request-interceptor/request-interceptor.service';
import { HttpRequestService } from './http-request/http-request.service';
import { AuthenticationService } from './authentication/authentication.service';
import { AuthorizationService } from './authorization/authorization.service';
import { SingleSignOnService } from './single-sign-on/single-sign-on.service';
import { VersionService } from './environment-config/version.service';
import { EnvironmentConfigService } from './environment-config/environment-config.service';
import { ErrorHandlerInterceptor } from './error-handling/error-handler.interceptor';
import { ErrorAlertModule } from './error-alert/error-alert.module';
import { HasPermissionDirective } from './authorization/has-permission.directive';

// --------------------------------------- //
export { RequestInterceptorService } from './request-interceptor/request-interceptor.service';
export { Signature } from './authentication/user.model';
export { ErrorHandlerInterceptor } from './error-handling/error-handler.interceptor';
export { HttpRequestService } from './http-request/http-request.service';
export { AuthenticationService } from './authentication/authentication.service';
export { AuthorizationService } from './authorization/authorization.service';
export { SingleSignOnService } from './single-sign-on/single-sign-on.service';
export { EnvironmentConfigService } from './environment-config/environment-config.service';
export { VersionService } from './environment-config/version.service';
export { singleSignOn } from './authentication/single-sign-on';
export * from './error-alert/index';

@NgModule({
    imports: [
        CommonModule,
        ErrorAlertModule.forRoot()
    ],
    declarations: [
        HasPermissionDirective
    ],
    exports: [
        HasPermissionDirective
    ]
})
export class CoreModule {
    /**
     * Confirms that the core module is initialized once and only once in the AppModule. If it is loaded again by any of the lazy loaded
     * feature module or a feature module than the application will terminate with an error.
     * @param parentModule The module to be injected
     */
    constructor(@Optional() @SkipSelf() parentModule: CoreModule) {
        if (parentModule) {
            throw new Error('Core module is already loaded. It should only be imported in AppModule only');
        }
    }

    /**
   * This static method is used to load all the services registered with core module.
   */
    static forRoot(): ModuleWithProviders {
        return {
            ngModule: CoreModule,
            providers: [
                RequestInterceptorService,
                ErrorHandlerInterceptor,
                HttpRequestService,
                SingleSignOnService,
                VersionService,
                AuthenticationService,
                AuthorizationService,
                EnvironmentConfigService,
                ErrorHandlerInterceptor
            ]
        }
    }
}
