/**
 * @author Faizal Vasaya
 * This service is used to initialize and get environment specific url, environment name and version.
 */
import { Injectable } from '@angular/core';
import { Subject } from 'rxjs/Subject';
// --------------------------------------- //
import { EnvironmentConfig } from './environment-config.model';
import { VersionService } from './version.service';

@Injectable()
export class EnvironmentConfigService {
  public environment: EnvironmentConfig;
  public environmentDetails: Subject<EnvironmentConfig>;
  constructor(private version: VersionService) {
    this.environmentDetails = new Subject<EnvironmentConfig>();
    this.environment = new EnvironmentConfig();
  }

  /**
   * Initializes the environment variables based on the current url in the browser's address bar. After succesfull initialization, it emits the environment details
   * to the components who has subscribed to it environmentDetails
   * @param url The current url in the browser's address bar.
   */
  initializeApplicationEnvironment(url: string) {

    let environmentName: string = '';
    let rolePrefix: string = '';

    // Special condition to support development on localhost. Fetches the url from address bar
    if (url.includes('localhost')) {
      this.environment.baseUrl = 'http://nts-vdmp1-soa';
    } else {
      this.environment.baseUrl = 'http://' + url;
    }

    // Assigns the names of the servers based on the url in the address bar
    if (url.includes('localhost') || url.includes('d1')) {
      environmentName = 'SIT';
      rolePrefix = 'd_'
    } else if (url.includes('t1') || url.includes('t2')) {
      environmentName = 'UAT';
      rolePrefix = 'd_'
    } else if (url.includes('prod')) {
      environmentName = 'PROD';
      rolePrefix = 'p_'
    }

    this.environment.name = environmentName;
    this.environment.rolePrefix = rolePrefix;
    this.environment.version = this.version.number;
    this.emitEnvironmentDetails(this.environment);
  }

  /**
   * Emits environment details which is passed as the parameter
   * @param environment The environment object to be emitted
   */
  emitEnvironmentDetails(environment: EnvironmentConfig) {
    this.environmentDetails.next(environment);
  }
}
