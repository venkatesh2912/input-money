import { TestBed, inject } from '@angular/core/testing';

import { EnvironmentConfigService } from './environment-config.service';

describe('EnvironmentConfigService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [EnvironmentConfigService]
    });
  });

  it('should be created', inject([EnvironmentConfigService], (service: EnvironmentConfigService) => {
    expect(service).toBeTruthy();
  }));
});
