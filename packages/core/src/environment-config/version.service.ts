/**
 * @author Faizal Vasaya
 * This service is used to initialize version.
 */
import { Injectable } from '@angular/core';

@Injectable()
export class VersionService {
    public number: string;
    constructor() {

    }

    setVersion(version: string) {
        this.number = version;
    }
}
