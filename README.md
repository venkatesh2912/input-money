# @fhlbny-ui-commons
> This repositorty consists of multiple reusable npm libraries required for fhlbny angular applicaitons.

# [Examples](./EXAMPLES.md)

## Table of contents
======================================================================================================
1. Dependencies
2. Installation using local link
3. Installation using artifactory
4. @fhlbny-ui-commons/core
5. @fhlbny-ui-commons/components
6. @fhlbny-ui-commons/navbar
7. @fhlbny-ui-commons/pipes

## 1. Dependencies
These dependecies should be installed at a global level:

| npm library/software | Version |
| ------ | ------ |
| npm | 3.10.10 |
| node | 6.11.2 |
| angular-cli | 1.6.8 |
| gulp | 3.9.1 |
| ng-packagr | 2.4.2 |

## 2. Installation using local link
> Detailed guideline on how to get fhlbny-ui-commons up and running in local environment.
Knowledge of ```gulp``` and ```npm link``` would be helpful
```sh
Perform npm install and continue to step 1.

Step 1: gulp build:core
> This should create a dist/core in the current directory
Step 2: gulp link:core
Step 3: gulp build:pipes
> This should create a dist/pipes in the current directory
Step 4: gulp link:pipes
Step 5: cd packages/components
Step 6: npm install
> This should create a node_modules folder in packages/components
Step 7: npm link @fhlbny-ui-commons/core, cd ../..
> This should add a symlink for @fhlbny-ui-commons/core in node_modules folder in the packages/components directory
Step 8: gulp build:components
> This should create a dist/components in the current directory
Step 9: gulp link:components
Step 10: cd packages/utils
Step 11: npm install
Step 12: npm link @fhlbny-ui-commons/components, cd ../..
> This should add a symlink for @fhlbny-ui-commons/components in node_modules folder in the packages/components directory
Step 13: gulp build:utils
Step 14: gulp link:utils
Step 15: cd packages/navbar
Step 16: npm install
> This should create a node_modules folder in packages/navbar
Step 17: npm link @fhlbny-ui-commons/pipes
Step 18: npm link @fhlbny-ui-commons/components
Step 19: npm link @fhlbny-ui-commons/core
>  This should add a symlink for @fhlbny-ui-commons/core, @fhlbny-ui-commons/components, @fhlbny-ui-commons/pipes in node_modules folder in the packages/navbar directory
Step 20: cd ../..
> This makes sure that the current working directory is fhlbny-ui-commons
Step 21: gulp build:navbar
Step 22: gulp link:navbar
Step 23: Re-run commands in step 17, step 18 and step 19
Step 24: npm link @fhlbny-ui-commons/navbar
Step 25: npm link @fhlbny-ui-commons/utils
>  The above commands should add a symlink for @fhlbny-ui-commons/core, @fhlbny-ui-commons/components, @fhlbny-ui-commons/pipes, @fhlbny-ui-commons/navbar and @fhlbny-ui-commons/utils in node_modules folder of fhlbny-ui-commons directory.
Step 26: ng serve
> This runs the demo application.
```

## 3. Installation using artifactory
> To be done when artifactory will be provided

## 4. @fhlbny-ui-commons/core
> This package consists of services that are common for all the fhlbny angular applications. Code for this package can be found in packages/core directory.

| Service | Use |
| ------ | ------ |
| ```AuthenticationService``` | Manages user authentication |
| ```AuthorizationService``` | Provides route guards based on permissions |
| ```EnvironmentConfigService``` | Configures environment in runtime |
| ```HttpRequestService``` | Contains boiler plate code to invoke CRUD API endpoints |
| ```RequestInterceptorService``` | Intercepts all the outgoing and incoming requests |
| ```SingleSignOnService``` | Used to show hide single signon popup window |

> Refer demo application for their usage.

## 5. @fhlbny-ui-commons/components
> This package consists of components that are common for all the fhlbny angular applications. Code for this package can be found in packages/components directory.

#### Follow the below steps to create  new component in this package
1. Create new folder with the name of the component in packages/components/src directory
2. Create following files in <component-name> folder.
    * ```<component-name>.component.ts```
    * ```<component-name>.module.ts```
    * ```<component-name>.model.ts``` (optional)
    * ```<component-name>.service.ts``` (optional)
    * ```index.ts```
    * ```<component-name>.scss``` (optional)

3. Register component, service in ```<component-name>.module.ts```
4. Re-export all the files exposed to the outside world in ```index.ts```
5. Re-export this index.ts in ```components.module.ts```

Lookout for ```<fhlbny-toaster>``` component for a sample solution

#### Following specifies the components registered in this package
| Component | Use |
| ------ | ------ |
| ```fhlbny-toaster``` | Provides a toast service that can be used to show toast messages on the top-right. |
| ```fhlbny-spinner``` | Provides spinner service to show spinner when data loads from the server. |
| ```fhlbny-panel``` | Wrap list/Add/Search and Edit screens using this panel to show a panel. |
| ```LoginComponent``` | Register a route to this component for all the applications that require a custom login. |
| ```fhlbny-form``` | Renders a dynamic form based on the form description passed in it. See how to use for more details. |
| ```fhlbny-string-input``` | Renders a textbox for string input. |
| ```fhlbny-number-input``` | Renders a textbox for number input. |
| ```fhlbny-select-input``` | Renders a dropdown based on the values passed in its data field. |
| ```fhlbny-date-input``` | Renders a date input box with a pop up calendar. |
| ```fhlbny-filtering``` | Renders a search form based on its configuration. See how to use for more details. |
| ```fhlbny-export-controls``` | Contains the export buttons. |
| ```fhlbny-table``` | Renders a list table with pagination and sorting support. |
| ```fhlbny-modal``` | Renders a modal with specified body and footer elements. |

#### Limitations of fhlbny-form component
* It can only be used with reactive forms.
* The look and feel of the form cannot be altered.

## 6. @fhlbny-ui-commons/navbar
> This package consists of navbar used for all the fhlbny angular applications. It contains ```<fhlbny-topbar>```, ```<fhlbny-user-menu>```, ```<fhlbny-notification>``` and  ```<fhlbny-menu>``` components.

> Refer demo application for their usage.

## 7. @fhlbny-ui-commons/pipes
> This package consists of pipes for all the fhlbny angular applications. As of now, it has ```search``` pipe registered in it.

## 8. @fhlbny-ui-commons/utils
> This package consists of utility methods. It should contain only static methods in their respective classes.

### It has the following utiltites in it
| Utility | Use |
| ------ | ------ |
| ```QueryParamsStateService``` | Provides utility method to extract object and set query params in the url. |


# select checkbox package file location
# package file location packages\components\src\form\select-checkbox