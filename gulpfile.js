const gulp = require('gulp');
var gulpSequence = require('gulp-sequence');
const run = require('gulp-run');
const del = require('del');
var fs = require('fs');
var path = require('path');
var spawn = require('child_process').spawn


/**
 * Directory configuration
 */
const config = {
    inputDir: 'packages',
    outputDir: 'dist/',
    navbarDir: 'dist/navbar',
    coreDir: 'dist/core',
    pipesDir: 'dist/pipes',
    utilsDir: 'dist/utils',
    componentsDir: 'dist/components'
}

/**
 * Returns a list of folders in the specified directory
 * @param {*} dir The directory in which the folders are to be listed.
 */
function getFolders(dir) {
    return fs.readdirSync(dir)
        .filter(function (file) {
            return fs.statSync(path.join(dir, file)).isDirectory();
        });
}

/** --------------- NAVBAR ---------------------*/
/**
 * Build's the navbar library
 */
gulp.task('build:navbar', ['clean:navbar'], () => {
    return run(`ng-packagr -p ${config.inputDir}/navbar/package.json`).exec();
});

/**
 * Cleans the content of dist/navbar folder.
 */
gulp.task('clean:navbar', () => {
    return del(`${config.navbarDir}**/*`);
});

/**
 * Link navbar library
 */
gulp.task('link:navbar', () => {
    spawn('npm.cmd', ['link'], { cwd: config.navbarDir, stdio: 'inherit' });
});

/**
 * watches only navbar for changes
 */
gulp.task('watch:navbar', () => {
    gulp.watch(
        [
            'packages/navbar/**/*.ts',
            'packages/navbar/**/*.html',
            'packages/navbar/**/*.scss'
        ],
        [
            'build:navbar'
        ]
    );
});
/** ---------------  END OF NAVBAR ---------------------*/


/** --------------- CORE ---------------------*/
/**
 * Build's the core library
 */
gulp.task('build:core', ['clean:core'], () => {
    return run(`ng-packagr -p ${config.inputDir}/core/package.json`).exec();
});


/**
 * Cleans the content of dist/core folder.
 */
gulp.task('clean:core', () => {
    return del(`${config.coreDir}**/*`);
});

/**
 * Link core library
 */
gulp.task('link:core', () => {
    spawn('npm.cmd', ['link'], { cwd: config.coreDir, stdio: 'inherit' });
});

/**
 * watches only core for changes
 */
gulp.task('watch:core', () => {
    gulp.watch(
        [
            'packages/core/**/*.ts',
            'packages/core/**/*.html',
            'packages/core/**/*.scss'
        ],
        [
            'build:core'
        ]
    );
});

/** ---------------  END OF CORE ---------------------*/


/** --------------- PIPES ---------------------*/
/**
 * Build's the pipes library
 */
gulp.task('build:pipes', ['clean:pipes'], () => {
    return run(`ng-packagr -p ${config.inputDir}/pipes/package.json`).exec();
});

/**
 * Cleans the content of dist/pipes folder.
 */
gulp.task('clean:pipes', () => {
    return del(`${config.pipesDir}**/*`);
});

/**
 * Link pipes library
 */
gulp.task('link:pipes', () => {
    spawn('npm.cmd', ['link'], { cwd: config.pipesDir, stdio: 'inherit' });
});

/**
 * watches only pipe for changes
 */
gulp.task('watch:pipes', () => {
    gulp.watch(
        [
            'packages/pipes/**/*.ts',
            'packages/pipes/**/*.html',
            'packages/pipes/**/*.scss'
        ],
        [
            'build:pipes'
        ]);
});


/** ---------------  END OF PIPES ---------------------*/

/** --------------- UTILS ---------------------*/
/**
 * Build's the utils library
 */
gulp.task('build:utils', ['clean:utils'], () => {
    return run(`ng-packagr -p ${config.inputDir}/utils/package.json`).exec();
});

/**
 * Cleans the content of dist/utils folder.
 */
gulp.task('clean:utils', () => {
    return del(`${config.utilsDir}**/*`);
});

/**
 * Link utils library
 */
gulp.task('link:utils', () => {
    spawn('npm.cmd', ['link'], { cwd: config.utilsDir, stdio: 'inherit' });
});

/**
 * watches only pipe for changes
 */
gulp.task('watch:utils', () => {
    gulp.watch(
        [
            'packages/utils/**/*.ts',
            'packages/utils/**/*.html',
            'packages/utils/**/*.scss'
        ],
        [
            'build:utils'
        ]);
});


/** ---------------  END OF UTILS ---------------------*/

/** --------------- COMPONENTS ---------------------*/
/**
 * Build's the components library
 */
gulp.task('build:components', ['clean:components'], () => {
    return run(`ng-packagr -p ${config.inputDir}/components/package.json`).exec();
});

/**
 * Cleans the content of dist/components folder.
 */
gulp.task('clean:components', () => {
    return del(`${config.componentsDir}**/*`);
});

/**
 * Link components library
 */
gulp.task('link:components', () => {
    spawn('npm.cmd', ['link'], { cwd: config.componentsDir, stdio: 'inherit' });

});

/**
 * watches only component for changes
 */
gulp.task('watch:components', () => {
    gulp.watch(
        [
            'packages/component/**/*.ts',
            'packages/component/**/*.html',
            'packages/component/**/*.scss'
        ],
        [
            'build:components'
        ]);
});


/** ---------------  END OF COMPONENTS ---------------------*/

/** ------------ CLEAN ALL THE BUILDS --------------  */
gulp.task('clean:all', () => {
    var folders = getFolders(config.inputDir);
    folders.forEach(
        (folder) => del(`dist/${folder}**/*`)
    )
    del(`${config.outputDir}*.tgz`);
});
/** ------------ END OF CLEAN ALL THE BUILDS --------------  */

// Doesn't work. In progress
gulp.task('default', gulpSequence('clean:all', 'build:core', 'link:core', 'build:pipes', 'link:pipes', 'build:components', 'link:components', 'build:navbar', 'link:navbar', 'watch'));


