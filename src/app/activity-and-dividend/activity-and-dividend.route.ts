import { Routes } from '@angular/router';
import { ActivityAndDividendComponent } from './activity-and-dividend.component';
import { AuthorizationService } from '@fhlbny-ui-commons/core';
export const ActivityAndDividendRoute: Routes = [
    {
        path: 'activity-and-dividend',
        component: ActivityAndDividendComponent,
        data: {
            breadcrumb: ['Home', 'Inq/Reports', 'Activity and Dividend']
        },
        canActivate: [AuthorizationService]
    }
];


