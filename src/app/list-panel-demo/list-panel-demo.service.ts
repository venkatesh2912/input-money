import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
// --------------------------------------- //
import { HttpRequestService } from '@fhlbny-ui-commons/core';

@Injectable()
export class ListDemoService {
  public url: string;
  constructor(private httpRequest: HttpRequestService) {
    this.url = '/api/test';
  }

  /**
   * Invokes application getRequest to fetch the list of acs to gl records.
   */
  getdata(): Observable<any> {
    return Observable.create(
      (observer) => {
        // Invokes the getRequest method and subscribe to its promise.
        this.httpRequest.getRequest<any>(this.url, "User Services").then(
          // If the promise was resolved successfully.
          (response) => {
            observer.next(response.records);
            observer.complete();
          },
          // If the promise was rejected in case of any error.
          (error) => {
            observer.error(error);
            observer.complete();
          }
        );
      });
  }

  postData(data): Observable<any> {
    return Observable.create(
      (observer) => {
        // Invokes the getRequest method and subscribe to its promise.
        this.httpRequest.postRequest<any>('/api/stk-classes', data, "User Services").then(
          // If the promise was resolved successfully.
          (response) => {
            observer.next(response.records);
            observer.complete();
          },
          // If the promise was rejected in case of any error.
          (error) => {
            observer.error(error);
            observer.complete();
          }
        );
      });
  }
}