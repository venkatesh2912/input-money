/**
 * @author Faizal Vasaya
 * @class  ListPanelDemoComponent
 * The ListPanelDemoComponent to demo the use of reusable panel component for list screen
 */
import { Component, OnInit, ViewChild, AfterViewInit } from '@angular/core';
import { Location } from '@angular/common';

// --------------------------------------- //
import { ToasterService, ToasterType, ToasterTitle, FilterField, TableField, SortConfig, PageData, FormType, FormDescription, ButtonType, InputType, FilteringComponent } from '@fhlbny-ui-commons/components';
import { ActivatedRoute, Router } from '@angular/router';
import { StateParam, QueryParamsStateService } from '@fhlbny-ui-commons/utils';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import { HttpRequestService } from '@fhlbny-ui-commons/core';
import { ListDemoService } from './list-panel-demo.service';


@Component({
    selector: 'list-panel-demo',
    templateUrl: './list-panel-demo.component.html'
})

export class ListPanelDemoComponent implements OnInit, AfterViewInit {

    @ViewChild(FilteringComponent) filterComponent: FilteringComponent;
    demoForm: FormGroup;
    formDescription: FormDescription[];
    formType: FormType

    fields: any[] = [{
        name: 'id',
        label: 'ID',
        inputType: 'number',
        routerLinkFn: (item) => `../entry/${item.id}`
    }, {
        name: 'title',
        label: 'Title'
    }, {
        name: 'content',
        label: 'Content'
    }, {
        name: 'blog.name',
        label: 'Blog'
    }];

    filterFields: FilterField[] = this.fields;
    tableFields: TableField[] = this.fields;

    items: any[] = [
        { title: 'A blog', content: 'A blogs content', name: 'A blogs name' },
        { title: 'B blog', content: 'B blogs content', name: 'B blogs name' }
    ];

    sortConfig: SortConfig = {
        ascending: true,
        callback: this.reset.bind(this),
        predicate: 'id'
    };

    pageData: PageData = {
        currentPage: 0,
        totalPages: 2,
        totalItems: 25,
        itemsPerPage: 10,
        numItemsOnCurrentPage: 10
    };
    searchObject: object = {};

    constructor(
        private toaster: ToasterService,
        private _route: ActivatedRoute,
        private _router: Router,
        private activatedRoute: ActivatedRoute,
        private location: Location,
        private fb: FormBuilder,
        private listDemoService: ListDemoService
    ) { }

    ngOnInit() {
        // Subscribe to query params to get the details of
        this._route.queryParams.subscribe((params) => {
            if (Object.keys(params).length !== 0) {
                Object.keys(params).forEach(
                    (keyName, index, array) => {
                        switch (keyName) {
                            case 'itemsPerPage':
                                this.pageData.itemsPerPage = +params[keyName];
                                break;
                            case 'page':
                                this.pageData.currentPage = +params[keyName];
                                break;
                            case 'ascending':
                                this.sortConfig.ascending = params[keyName];
                                break;
                            case 'predicate':
                                this.sortConfig.predicate = params[keyName];
                                break;
                            default:
                                this.fields.forEach(
                                    (field, index1, array) => {
                                        if (keyName.substr(0, keyName.lastIndexOf(".")) === field.name) {
                                            this.filterFields[index1]['value'] = params[keyName];
                                        }
                                    });
                        }
                    });
            }
        });

        this.createForm();
    }

    ngAfterViewInit() {
        this.searchObject = this.filterComponent.getSearchObject();
    }

    reset() {
        this.pageData.currentPage = 0;
        this.items = [];
        this.changeStateParams();
    }

    loadPage(page) {
        this.pageData.currentPage = page + 1;
        this.changeStateParams();
    }

    addNewRecord() {
        this.toaster.toast({
            message: 'Add button clicked.',
            type: ToasterType.Success,
            title: ToasterTitle.Success
        });
    }

    refreshData() {
        this.toaster.toast({
            message: 'Refresh button clicked.',
            type: ToasterType.Success,
            title: ToasterTitle.Success
        });
    }

    exportClicked(type) {
        this.toaster.toast({
            message: type + ' export button clicked.',
            type: ToasterType.Success,
            title: ToasterTitle.Success
        });
    }

    onPageSizeChange(size: number) {
        this.toaster.toast({
            message: size + ' size selected.',
            type: ToasterType.Success,
            title: ToasterTitle.Success
        });

        this.pageData.itemsPerPage = size;
        this.changeStateParams();
    }

    onSearchSubmit(searchObject: object) {
        this.searchObject = searchObject;
        this.changeStateParams();
    }

    /**
     * Creates object that will be served as input to createQueryParamsForCurrentUrl
     */
    changeStateParams() {
        if (this.searchObject !== undefined) {
            QueryParamsStateService.setQueryParams(this.activatedRoute, this._router, this.pageData, this.sortConfig, this.searchObject);
        } else {
            QueryParamsStateService.setQueryParams(this.activatedRoute, this._router, this.pageData, this.sortConfig);
        }
    }

    delete(modal) {
        modal.open();
    }

    createForm() {
        this.demoForm = this.fb.group({
            numberInput: ['', [Validators.required]],
            stringInput: ['', [Validators.required]],
            dateInput: ['', [Validators.required]],
            selectInput: [null, [Validators.required]]
        });
        this.formDescription = [
            {
                label: "Number Input",
                control: this.demoForm.get('numberInput'),
                type: InputType.number,
                errorMessages: [
                    { error: 'required', message: 'Number Input is required' },
                    { error: 'pattern', message: 'Number Input is not a valid number' }
                ]
            },
            {
                label: "String Input",
                control: this.demoForm.get('stringInput'),
                type: InputType.string,
                errorMessages: [
                    { error: 'required', message: 'String Input is required' }
                ]
            },
            {
                label: "Date Input",
                control: this.demoForm.get('dateInput'),
                type: InputType.date,
                errorMessages: [
                    { error: 'required', message: 'Date Input is required' }
                ]
            },
            {
                label: "Select Input",
                control: this.demoForm.get('selectInput'),
                type: InputType.select,
                values: [{
                    id: 1,
                    value: 'Sample 1'
                },
                {
                    id: 2,
                    value: 'Sample 2'
                }],
                errorMessages: [
                    { error: 'required', message: 'Select Input is required' }
                ]
            }
        ];
        this.demoForm.patchValue(
            {
                numberInput: '1234',
                stringInput: 'Hey',
                dateInput: '12/04/2018',
                selectInput: {
                    id: 1,
                    value: 'Sample 1'
                }
            }
        )
    }

    btnClick() {
        this.listDemoService.getdata().subscribe((result) => {
        },
            (err) => { });
    }

    btnClickPost() {
        this.listDemoService.postData({
            "classCode": null,
            "classDesc": "CCC",
            "stkYrDaysCode": "360"
        }).subscribe((result) => {
        },
            (err) => { });
    }

    buttonClicked(event: ButtonType) {
        if (event === ButtonType.add) {
            console.log(this.demoForm.value);
        } else if (event === ButtonType.update) {
            console.log(this.demoForm.value);
        } else if (event === ButtonType.cancel) {
            console.log('Cancel was clicked');
        }
    }

    routeToEdit() {
        this._router.navigate(['/edit'], { queryParamsHandling: "preserve" });
    }

    closeDeleteModal(deleteModal: any) {
        deleteModal.close();
    }
    openDeleteModal(deleteModal: any) {
        deleteModal.open();
    }
}
