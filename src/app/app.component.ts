import { Component, OnInit, AfterViewInit } from '@angular/core';
import { Router } from '@angular/router';

import { appName, mainMenuList } from './topbar-demo/topbar.config';
import { MenuModel } from '@fhlbny-ui-commons/navbar';
import { AuthenticationService, EnvironmentConfigService } from '@fhlbny-ui-commons/core';
import { ToasterService, ToasterType, ToasterTitle } from '@fhlbny-ui-commons/components';
import { SpinnerService } from '@fhlbny-ui-commons/components';
import {FormControl, FormGroup, FormBuilder, Validators } from '@angular/forms';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.scss']
})

export class AppComponent implements OnInit, AfterViewInit {
    form: FormGroup;
    money: FormControl;
    data = [
        {
            'key': 'cat',
            'label': 'category',
            'data': [
                {
                    'id': '1',
                    'name': '1'
                },
                {
                    'id': '2',
                    'name': '2'
                }
            ]
        },
        {
            'key': 'subcat',
            'label': 'sub-category',
            'parent': 'cat',
            'data': [
                {
                    'id': '11',
                    'name': '11',
                    'parentid': '1'
                },
                {
                    'id': '12',
                    'name': '12',
                    'parentid': '1'
                }
            ]
        },
        {
            'key': 'subcat1',
            'label': 'sub-category1',
            'data': [
                {
                    'id': '111',
                    'name': '111',
                },
                {
                    'id': '122',
                    'name': '122',
                }
            ]
        }
    ];
    public appName: string;
    public mainMenuList: MenuModel[];
    public showTopbar: boolean;
    constructor(
        private authService: AuthenticationService,
        private environmentConfigService: EnvironmentConfigService,
        private router: Router,
        private toaster: ToasterService,
        private spinner: SpinnerService,
        private fb: FormBuilder
    ) {
        this.appName = appName;
        this.mainMenuList = mainMenuList;
        this.form = fb.group({
            'price' : new FormControl('', []),
            'cat' : new FormControl('', []),
            'subcat' : new FormControl('', []),
            'subcat1' : new FormControl('', []),
            'system': this.fb.group({ // make a nested group
                'values' : new FormControl('', []),
                'search' : new FormControl('', [])
            }),
        });
    }

    ngOnInit() {
        // To get values to decide whether to show menu or not
        this.showTopbar = this.authService.user.isAuthenticated && this.authService.user.isAuthorized;
        // TO get the userdetails initialized using custom login
        this.authService.userDetails.subscribe(
            (userDetails) => {
                this.showTopbar = userDetails.isAuthenticated && userDetails.isAuthorized;
            }
        );
        //this.showTopbar = true;
        // CALL WHEN search has a value
        this.form.controls.system.get('search').
        valueChanges.
        subscribe(form => {
            console.log('call log', form);
            if (form !== '') {
                this.source = this.source.filter(item =>
                (  new RegExp(form, 'gi').test(item['name'])) || form === '', true);
            } else {
                this.source = this.tube;
            }
        });
    }

    ngAfterViewInit() {
        //Demo for toast
        this.toaster.toast({
            message: 'Hey',
            type: ToasterType.Success,
            title: ToasterTitle.Success
        });
        // Demo for spinner
        this.spinner.spin(true);
        setTimeout(
            () => {
                this.spinner.spin(false);
            }, 3000
        )
    }
    
    /**
    * The method invoked when the userloggedout event occurs from user-menu child component.
    */
    onUserLoggedOut($event) {
        this.router.navigate(['/login']);
    }
    tube = [
		{'id': '1','name': 'Harrow & Wealdstone'},
		{'id': '2','name':'Kenton'},
		{'id':'3','name':'South Kenton'},
		{'id':'4','name':'North Wembley'},
		{'id':'5','name':'Wembley Central'},
		{'id':'6','name': 'Harrow & Wealdstone'},
		{'id':'7','name':'Kenton'},
		{'id':'8','name':'South Kenton'},
		{'id':'9','name':'North Wembley'},
		{'id':'10','name':'Wembley Central'},
		{'id':'11','name': 'Harrow & Wealdstone'},
		{'id':'12','name':'Kenton'},
		{'id':'13','name':'South Kenton'},
		{'id':'14','name':'North Wembley'},
		{'id':'15','name':'Wembley Central'},
		{'id':'16','name': 'Harrow & Wealdstone'},
		{'id':'17','name':'Kenton'},
		{'id':'18','name':'South Kenton'},
		{'id':'19','name':'North Wembley'},
		{'id':'20','name':'Wembley Central'},
		{'id':'21','name': 'Harrow & Wealdstone'},
		{'id':'22','name':'Kenton'},
		{'id':'23','name':'South Kenton'},
		{'id':'24','name':'North Wembley'},
		{'id':'25','name':'Wembley Central'},
		{'id':'26','name': 'Harrow & Wealdstone'},
		{'id':'27','name':'Kenton'},
		{'id':'28','name':'South Kenton'},
		{'id':'29','name':'North Wembley'},
		{'id':'30','name':'Wembley Central'},
	];
	/* tslint:enable quotemark */

	target = [];
	source =this.tube;
}
