### Below is an example of a JHipster list screen:

#### entries.component.html
    <div>
        <h2>
            <span jhiTranslate="blogApp.entry.home.title">Entries</span>
        </h2>
        <jhi-alert></jhi-alert>

        <fhlbny-panel headerTitle="Search Controls">
            <fhlbny-filtering
                [fields]="filterFields"
                (onSearchSubmit)="onSearchSubmit($event)"
            ></fhlbny-filtering>
        </fhlbny-panel>

        <fhlbny-panel headerTitle="Entries List">
            <ng-template #additionalHeaderContent>
                <div>
                    <fhlbny-page-size-selector class="d-inline-block align-middle mr-2"
                        [currentPageSize]="pageData.itemsPerPage"
                        (onPageSizeChange)="onPageSizeChange($event)"
                    ></fhlbny-page-size-selector>

                    <fhlbny-export-controls (onExport)="onExportClicked($event)"></fhlbny-export-controls>

                    <button type="button" class="btn btn-secondary" (click)="reset()">
                        <i class="fa fa-refresh"></i> Refresh
                    </button>

                    <button type="button" class="btn btn-secondary" [routerLink]="['/', { outlets: { popup: ['entry-new'] } }]">
                        <i class="fa fa-plus"></i> Add
                    </button>
                </div>
            </ng-template>

            <fhlbny-table
                [fields]="tableFields"
                [items]="items"
                [pageData]="pageData"
                (onPageChange)="loadPage($event)"
                [sortConfig]="sortConfig"
            ></fhlbny-table>
        </fhlbny-panel>
    </div>

### entries.component.ts
    import { Component, OnInit, OnDestroy } from '@angular/core';
    import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
    import { Subscription } from 'rxjs/Subscription';
    import { JhiEventManager, JhiParseLinks, JhiAlertService, JhiDataUtils } from 'ng-jhipster';

    import { Entry } from './entry.model';
    import { EntryService } from './entry.service';
    import { ITEMS_PER_PAGE, Principal } from '../../shared';

    import {
        ExportType,
        FilterField,
        PageData,
        SortConfig
        TableField
    } from '@fhlbny-ui-commons/components';

    @Component({
        selector: 'jhi-entry',
        templateUrl: './entry.component.html'
    })
    export class EntryComponent implements OnInit, OnDestroy {

        currentAccount: any;
        eventSubscriber: Subscription;

        fields: any[] = [{
            name: 'id',
            label: 'ID',
            inputType: 'number',
            routerLinkFn: (item) => `../entry/${item.id}`
        }, {
            name: 'title',
            label: 'Title',
            value: 'Prefilled value',
            maxLength: 25
        }, {
            name: 'content',
            label: 'Content',
            minLength: 3
        }, {
            name: 'date',
            label: 'Date',
            dataType: 'date',
            inputType: 'dateBetween',
            value: {
                // Date object
                to: new Date(),
                // or string
                from: '2018-05-03T04:00:00.000Z'
            }
        }, {
            name: 'blog.name',
            label: 'Blog'
        }];

        filterFields: FilterField[] = this.fields;
        tableFields: TableField[] = this.fields;

        items: Entry[] = [];

        pageData: PageData = {
            currentPage: 0,
            totalPages: 0,
            totalItems: 0,
            itemsPerPage: ITEMS_PER_PAGE,
            numItemsOnCurrentPage: 0
        };

        sortConfig: SortConfig = {
            ascending: true,
            callback: this.reset.bind(this),
            predicate: 'id'
        };

        searchObject: object;

        constructor(
            private entryService: EntryService,
            private jhiAlertService: JhiAlertService,
            private dataUtils: JhiDataUtils,
            private eventManager: JhiEventManager,
            private parseLinks: JhiParseLinks,
            private principal: Principal
        ) {}

        loadAll() {
            const { currentPage, itemsPerPage } = this.pageData;

            this.entryService.query({
                page: currentPage,
                size: itemsPerPage,
                sort: this.getSortString(),
                ...this.searchObject
            }).subscribe(
                (res: HttpResponse<Entry[]>) => this.onSuccess(res.body, res.headers),
                (res: HttpErrorResponse) => this.onError(res.message)
            );
        }

        reset() {
            this.pageData.currentPage = 0;
            this.items = [];
            this.loadAll();
        }

        loadPage(page) {
            this.pageData.currentPage = page;
            this.loadAll();
        }

        ngOnInit() {
            this.loadAll();
            this.principal.identity().then((account) => {
                this.currentAccount = account;
            });
            this.registerChangeInEntries();
        }

        ngOnDestroy() {
            this.eventManager.destroy(this.eventSubscriber);
        }

        trackId(index: number, item: Entry) {
            return item.id;
        }

        byteSize(field) {
            return this.dataUtils.byteSize(field);
        }

        openFile(contentType, field) {
            return this.dataUtils.openFile(contentType, field);
        }

        registerChangeInEntries() {
            this.eventSubscriber = this.eventManager.subscribe('entryListModification', (response) => this.reset());
        }

        getSortString() {
            const result = [this.sortConfig.predicate + ',' + (this.sortConfig.ascending ? 'asc' : 'desc')];
            if (this.sortConfig.predicate !== 'id') {
                result.push('id');
            }
            return result;
        }

        onPageSizeChange(itemsPerPage: number) {
            this.pageData.itemsPerPage = itemsPerPage;
            this.loadAll();
        }

        onSearchSubmit(searchObject: object) {
            this.pageData.currentPage = 0;
            this.searchObject = searchObject;
            this.loadAll();
        }

        onExportClicked(exportType: ExportType) {
            console.log(`Export with type: "${exportType}" clicked`);
        }

        private onSuccess(data, headers) {
            const links = this.parseLinks.parse(headers.get('link'));

            this.items = data;
            this.pageData = {
                ...this.pageData,
                totalItems: parseInt(headers.get('X-Total-Count'), 10),
                totalPages: links.last,
                numItemsOnCurrentPage: data.length
            };
        }

        private onError(error) {
            this.jhiAlertService.error(error.message, null, null);
        }
    }
